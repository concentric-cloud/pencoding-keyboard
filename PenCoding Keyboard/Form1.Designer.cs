﻿namespace PenCoding_Keyboard
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.KEY_TAB = new System.Windows.Forms.Button();
            this.KEY_EXCLAMATION = new System.Windows.Forms.Button();
            this.KEY_AT = new System.Windows.Forms.Button();
            this.KEY_DOLLAR = new System.Windows.Forms.Button();
            this.KEY_HASH = new System.Windows.Forms.Button();
            this.KEY_BACKSPACE = new System.Windows.Forms.Button();
            this.KEY_ASTERISK = new System.Windows.Forms.Button();
            this.KEY_AMP = new System.Windows.Forms.Button();
            this.KEY_PERCENT = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.KEY_EQUALS = new System.Windows.Forms.Button();
            this.KEY_PLUS = new System.Windows.Forms.Button();
            this.KEY_UNDERSCORE = new System.Windows.Forms.Button();
            this.KEY_HYPHEN = new System.Windows.Forms.Button();
            this.KEY_CURLYCLOSE = new System.Windows.Forms.Button();
            this.KEY_CURLYOPEN = new System.Windows.Forms.Button();
            this.KEY_GREATERTHAN = new System.Windows.Forms.Button();
            this.KEY_LESSTHAN = new System.Windows.Forms.Button();
            this.KEY_PAROPEN = new System.Windows.Forms.Button();
            this.KEY_C19 = new System.Windows.Forms.Button();
            this.KEY_COMMA = new System.Windows.Forms.Button();
            this.KEY_PERIOD = new System.Windows.Forms.Button();
            this.KEY_SEMICOLON = new System.Windows.Forms.Button();
            this.KEY_COLON = new System.Windows.Forms.Button();
            this.KEY_SQCLOSE = new System.Windows.Forms.Button();
            this.KEY_SQOPEN = new System.Windows.Forms.Button();
            this.KEY_PARCLOSE = new System.Windows.Forms.Button();
            this.KEY_BACKTICK = new System.Windows.Forms.Button();
            this.KEY_C20 = new System.Windows.Forms.Button();
            this.KEY_3 = new System.Windows.Forms.Button();
            this.KEY_2 = new System.Windows.Forms.Button();
            this.KEY_1 = new System.Windows.Forms.Button();
            this.KEY_BACKSLASH = new System.Windows.Forms.Button();
            this.KEY_FORWARDSLASH = new System.Windows.Forms.Button();
            this.KEY_APOSTROPHE = new System.Windows.Forms.Button();
            this.KEY_DOUBLEQUOTE = new System.Windows.Forms.Button();
            this.KEY_C6 = new System.Windows.Forms.Button();
            this.KEY_C23 = new System.Windows.Forms.Button();
            this.KEY_PGDN = new System.Windows.Forms.Button();
            this.KEY_0 = new System.Windows.Forms.Button();
            this.KEY_PGUP = new System.Windows.Forms.Button();
            this.KEY_C10 = new System.Windows.Forms.Button();
            this.KEY_C9 = new System.Windows.Forms.Button();
            this.KEY_C8 = new System.Windows.Forms.Button();
            this.KEY_C7 = new System.Windows.Forms.Button();
            this.KEY_C1 = new System.Windows.Forms.Button();
            this.KEY_C22 = new System.Windows.Forms.Button();
            this.KEY_9 = new System.Windows.Forms.Button();
            this.KEY_8 = new System.Windows.Forms.Button();
            this.KEY_7 = new System.Windows.Forms.Button();
            this.KEY_C5 = new System.Windows.Forms.Button();
            this.KEY_C4 = new System.Windows.Forms.Button();
            this.KEY_C3 = new System.Windows.Forms.Button();
            this.KEY_C2 = new System.Windows.Forms.Button();
            this.KEY_PIPE = new System.Windows.Forms.Button();
            this.KEY_C21 = new System.Windows.Forms.Button();
            this.KEY_6 = new System.Windows.Forms.Button();
            this.KEY_5 = new System.Windows.Forms.Button();
            this.KEY_4 = new System.Windows.Forms.Button();
            this.KEY_TWOPIPE = new System.Windows.Forms.Button();
            this.KEY_NOTEQUALS = new System.Windows.Forms.Button();
            this.KEY_TWOEQUALS = new System.Windows.Forms.Button();
            this.KEY_TWODOUBLEQUOTE = new System.Windows.Forms.Button();
            this.KEY_C16 = new System.Windows.Forms.Button();
            this.KEY_ENTER = new System.Windows.Forms.Button();
            this.KEY_RIGHT = new System.Windows.Forms.Button();
            this.KEY_DOWN = new System.Windows.Forms.Button();
            this.KEY_LEFT = new System.Windows.Forms.Button();
            this.KEY_SPACE = new System.Windows.Forms.Button();
            this.KEY_C18 = new System.Windows.Forms.Button();
            this.KEY_C17 = new System.Windows.Forms.Button();
            this.KEY_C11 = new System.Windows.Forms.Button();
            this.KEY_C24 = new System.Windows.Forms.Button();
            this.KEY_END = new System.Windows.Forms.Button();
            this.KEY_UP = new System.Windows.Forms.Button();
            this.KEY_HOME = new System.Windows.Forms.Button();
            this.KEY_C15 = new System.Windows.Forms.Button();
            this.KEY_C14 = new System.Windows.Forms.Button();
            this.KEY_C13 = new System.Windows.Forms.Button();
            this.KEY_C12 = new System.Windows.Forms.Button();
            this.KEY_CTRLC = new System.Windows.Forms.Button();
            this.KEY_CTRLS = new System.Windows.Forms.Button();
            this.KEY_CTRLX = new System.Windows.Forms.Button();
            this.KEY_CTRLV = new System.Windows.Forms.Button();
            this.KEY_CTRLA = new System.Windows.Forms.Button();
            this.KEY_OPT = new System.Windows.Forms.Button();
            this.KEY_ESC = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // KEY_TAB
            // 
            this.KEY_TAB.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_TAB.Location = new System.Drawing.Point(0, 34);
            this.KEY_TAB.Name = "KEY_TAB";
            this.KEY_TAB.Size = new System.Drawing.Size(43, 35);
            this.KEY_TAB.TabIndex = 0;
            this.KEY_TAB.Text = "TAB";
            this.KEY_TAB.UseVisualStyleBackColor = false;
            this.KEY_TAB.Click += new System.EventHandler(this.KEY_TAB_Click);
            // 
            // KEY_EXCLAMATION
            // 
            this.KEY_EXCLAMATION.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_EXCLAMATION.Location = new System.Drawing.Point(42, 34);
            this.KEY_EXCLAMATION.Name = "KEY_EXCLAMATION";
            this.KEY_EXCLAMATION.Size = new System.Drawing.Size(43, 35);
            this.KEY_EXCLAMATION.TabIndex = 1;
            this.KEY_EXCLAMATION.Text = "!";
            this.KEY_EXCLAMATION.UseVisualStyleBackColor = true;
            this.KEY_EXCLAMATION.Click += new System.EventHandler(this.KEY_EXCLAMATION_Click);
            // 
            // KEY_AT
            // 
            this.KEY_AT.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_AT.Location = new System.Drawing.Point(84, 34);
            this.KEY_AT.Name = "KEY_AT";
            this.KEY_AT.Size = new System.Drawing.Size(43, 35);
            this.KEY_AT.TabIndex = 2;
            this.KEY_AT.Text = "@";
            this.KEY_AT.UseVisualStyleBackColor = true;
            this.KEY_AT.Click += new System.EventHandler(this.KEY_AT_Click);
            // 
            // KEY_DOLLAR
            // 
            this.KEY_DOLLAR.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_DOLLAR.Location = new System.Drawing.Point(168, 34);
            this.KEY_DOLLAR.Name = "KEY_DOLLAR";
            this.KEY_DOLLAR.Size = new System.Drawing.Size(43, 35);
            this.KEY_DOLLAR.TabIndex = 4;
            this.KEY_DOLLAR.Text = "$";
            this.KEY_DOLLAR.UseVisualStyleBackColor = true;
            this.KEY_DOLLAR.Click += new System.EventHandler(this.KEY_DOLLAR_Click);
            // 
            // KEY_HASH
            // 
            this.KEY_HASH.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_HASH.Location = new System.Drawing.Point(126, 34);
            this.KEY_HASH.Name = "KEY_HASH";
            this.KEY_HASH.Size = new System.Drawing.Size(43, 35);
            this.KEY_HASH.TabIndex = 3;
            this.KEY_HASH.Text = "#";
            this.KEY_HASH.UseVisualStyleBackColor = true;
            this.KEY_HASH.Click += new System.EventHandler(this.KEY_HASH_Click);
            // 
            // KEY_BACKSPACE
            // 
            this.KEY_BACKSPACE.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_BACKSPACE.Location = new System.Drawing.Point(336, 34);
            this.KEY_BACKSPACE.Name = "KEY_BACKSPACE";
            this.KEY_BACKSPACE.Size = new System.Drawing.Size(63, 35);
            this.KEY_BACKSPACE.TabIndex = 8;
            this.KEY_BACKSPACE.Text = "BACKSPC";
            this.KEY_BACKSPACE.UseVisualStyleBackColor = false;
            this.KEY_BACKSPACE.Click += new System.EventHandler(this.KEY_BACKSPACE_Click);
            // 
            // KEY_ASTERISK
            // 
            this.KEY_ASTERISK.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_ASTERISK.Location = new System.Drawing.Point(294, 34);
            this.KEY_ASTERISK.Name = "KEY_ASTERISK";
            this.KEY_ASTERISK.Size = new System.Drawing.Size(43, 35);
            this.KEY_ASTERISK.TabIndex = 7;
            this.KEY_ASTERISK.Text = "*";
            this.KEY_ASTERISK.UseVisualStyleBackColor = true;
            this.KEY_ASTERISK.Click += new System.EventHandler(this.KEY_ASTERISK_Click);
            // 
            // KEY_AMP
            // 
            this.KEY_AMP.Location = new System.Drawing.Point(252, 34);
            this.KEY_AMP.Name = "KEY_AMP";
            this.KEY_AMP.Size = new System.Drawing.Size(43, 35);
            this.KEY_AMP.TabIndex = 6;
            this.KEY_AMP.Text = "AMP";
            this.KEY_AMP.UseVisualStyleBackColor = true;
            this.KEY_AMP.Click += new System.EventHandler(this.KEY_AMP_Click);
            // 
            // KEY_PERCENT
            // 
            this.KEY_PERCENT.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_PERCENT.Location = new System.Drawing.Point(210, 34);
            this.KEY_PERCENT.Name = "KEY_PERCENT";
            this.KEY_PERCENT.Size = new System.Drawing.Size(43, 35);
            this.KEY_PERCENT.TabIndex = 5;
            this.KEY_PERCENT.Text = "%";
            this.KEY_PERCENT.UseVisualStyleBackColor = true;
            this.KEY_PERCENT.Click += new System.EventHandler(this.KEY_PERCENT_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.button10.Location = new System.Drawing.Point(336, 68);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(63, 35);
            this.button10.TabIndex = 16;
            this.button10.Text = "SWITCH";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // KEY_EQUALS
            // 
            this.KEY_EQUALS.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_EQUALS.Location = new System.Drawing.Point(294, 68);
            this.KEY_EQUALS.Name = "KEY_EQUALS";
            this.KEY_EQUALS.Size = new System.Drawing.Size(43, 35);
            this.KEY_EQUALS.TabIndex = 15;
            this.KEY_EQUALS.Text = "=";
            this.KEY_EQUALS.UseVisualStyleBackColor = true;
            this.KEY_EQUALS.Click += new System.EventHandler(this.KEY_EQUALS_Click);
            // 
            // KEY_PLUS
            // 
            this.KEY_PLUS.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_PLUS.Location = new System.Drawing.Point(252, 68);
            this.KEY_PLUS.Name = "KEY_PLUS";
            this.KEY_PLUS.Size = new System.Drawing.Size(43, 35);
            this.KEY_PLUS.TabIndex = 14;
            this.KEY_PLUS.Text = "+";
            this.KEY_PLUS.UseVisualStyleBackColor = true;
            this.KEY_PLUS.Click += new System.EventHandler(this.KEY_PLUS_Click);
            // 
            // KEY_UNDERSCORE
            // 
            this.KEY_UNDERSCORE.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_UNDERSCORE.Location = new System.Drawing.Point(210, 68);
            this.KEY_UNDERSCORE.Name = "KEY_UNDERSCORE";
            this.KEY_UNDERSCORE.Size = new System.Drawing.Size(43, 35);
            this.KEY_UNDERSCORE.TabIndex = 13;
            this.KEY_UNDERSCORE.Text = "_";
            this.KEY_UNDERSCORE.UseVisualStyleBackColor = true;
            this.KEY_UNDERSCORE.Click += new System.EventHandler(this.KEY_UNDERSCORE_Click);
            // 
            // KEY_HYPHEN
            // 
            this.KEY_HYPHEN.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_HYPHEN.Location = new System.Drawing.Point(168, 68);
            this.KEY_HYPHEN.Name = "KEY_HYPHEN";
            this.KEY_HYPHEN.Size = new System.Drawing.Size(43, 35);
            this.KEY_HYPHEN.TabIndex = 12;
            this.KEY_HYPHEN.Text = "-";
            this.KEY_HYPHEN.UseVisualStyleBackColor = true;
            this.KEY_HYPHEN.Click += new System.EventHandler(this.KEY_HYPHEN_Click);
            // 
            // KEY_CURLYCLOSE
            // 
            this.KEY_CURLYCLOSE.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_CURLYCLOSE.Location = new System.Drawing.Point(126, 68);
            this.KEY_CURLYCLOSE.Name = "KEY_CURLYCLOSE";
            this.KEY_CURLYCLOSE.Size = new System.Drawing.Size(43, 35);
            this.KEY_CURLYCLOSE.TabIndex = 11;
            this.KEY_CURLYCLOSE.Text = "}";
            this.KEY_CURLYCLOSE.UseVisualStyleBackColor = true;
            this.KEY_CURLYCLOSE.Click += new System.EventHandler(this.KEY_CURLYCLOSE_Click);
            // 
            // KEY_CURLYOPEN
            // 
            this.KEY_CURLYOPEN.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_CURLYOPEN.Location = new System.Drawing.Point(84, 68);
            this.KEY_CURLYOPEN.Name = "KEY_CURLYOPEN";
            this.KEY_CURLYOPEN.Size = new System.Drawing.Size(43, 35);
            this.KEY_CURLYOPEN.TabIndex = 10;
            this.KEY_CURLYOPEN.Text = "{";
            this.KEY_CURLYOPEN.UseVisualStyleBackColor = true;
            this.KEY_CURLYOPEN.Click += new System.EventHandler(this.KEY_CURLYOPEN_Click);
            // 
            // KEY_GREATERTHAN
            // 
            this.KEY_GREATERTHAN.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_GREATERTHAN.Location = new System.Drawing.Point(42, 68);
            this.KEY_GREATERTHAN.Name = "KEY_GREATERTHAN";
            this.KEY_GREATERTHAN.Size = new System.Drawing.Size(43, 35);
            this.KEY_GREATERTHAN.TabIndex = 9;
            this.KEY_GREATERTHAN.Text = ">";
            this.KEY_GREATERTHAN.UseVisualStyleBackColor = true;
            this.KEY_GREATERTHAN.Click += new System.EventHandler(this.KEY_GREATERTHAN_Click);
            // 
            // KEY_LESSTHAN
            // 
            this.KEY_LESSTHAN.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_LESSTHAN.Location = new System.Drawing.Point(0, 68);
            this.KEY_LESSTHAN.Name = "KEY_LESSTHAN";
            this.KEY_LESSTHAN.Size = new System.Drawing.Size(43, 35);
            this.KEY_LESSTHAN.TabIndex = 17;
            this.KEY_LESSTHAN.Text = "<";
            this.KEY_LESSTHAN.UseVisualStyleBackColor = true;
            this.KEY_LESSTHAN.Click += new System.EventHandler(this.KEY_LESSTHAN_Click);
            // 
            // KEY_PAROPEN
            // 
            this.KEY_PAROPEN.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_PAROPEN.Location = new System.Drawing.Point(0, 102);
            this.KEY_PAROPEN.Name = "KEY_PAROPEN";
            this.KEY_PAROPEN.Size = new System.Drawing.Size(43, 35);
            this.KEY_PAROPEN.TabIndex = 26;
            this.KEY_PAROPEN.Text = "(";
            this.KEY_PAROPEN.UseVisualStyleBackColor = true;
            this.KEY_PAROPEN.Click += new System.EventHandler(this.KEY_PAROPEN_Click);
            // 
            // KEY_C19
            // 
            this.KEY_C19.Location = new System.Drawing.Point(336, 102);
            this.KEY_C19.Name = "KEY_C19";
            this.KEY_C19.Size = new System.Drawing.Size(63, 35);
            this.KEY_C19.TabIndex = 25;
            this.KEY_C19.Text = "c19";
            this.KEY_C19.UseVisualStyleBackColor = true;
            this.KEY_C19.Click += new System.EventHandler(this.KEY_C19_Click);
            // 
            // KEY_COMMA
            // 
            this.KEY_COMMA.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_COMMA.Location = new System.Drawing.Point(294, 102);
            this.KEY_COMMA.Name = "KEY_COMMA";
            this.KEY_COMMA.Size = new System.Drawing.Size(43, 35);
            this.KEY_COMMA.TabIndex = 24;
            this.KEY_COMMA.Text = ",";
            this.KEY_COMMA.UseVisualStyleBackColor = true;
            this.KEY_COMMA.Click += new System.EventHandler(this.KEY_COMMA_Click);
            // 
            // KEY_PERIOD
            // 
            this.KEY_PERIOD.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_PERIOD.Location = new System.Drawing.Point(252, 102);
            this.KEY_PERIOD.Name = "KEY_PERIOD";
            this.KEY_PERIOD.Size = new System.Drawing.Size(43, 35);
            this.KEY_PERIOD.TabIndex = 23;
            this.KEY_PERIOD.Text = ".";
            this.KEY_PERIOD.UseVisualStyleBackColor = true;
            this.KEY_PERIOD.Click += new System.EventHandler(this.KEY_PERIOD_Click);
            // 
            // KEY_SEMICOLON
            // 
            this.KEY_SEMICOLON.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_SEMICOLON.Location = new System.Drawing.Point(210, 102);
            this.KEY_SEMICOLON.Name = "KEY_SEMICOLON";
            this.KEY_SEMICOLON.Size = new System.Drawing.Size(43, 35);
            this.KEY_SEMICOLON.TabIndex = 22;
            this.KEY_SEMICOLON.Text = ";";
            this.KEY_SEMICOLON.UseVisualStyleBackColor = true;
            this.KEY_SEMICOLON.Click += new System.EventHandler(this.KEY_SEMICOLON_Click);
            // 
            // KEY_COLON
            // 
            this.KEY_COLON.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_COLON.Location = new System.Drawing.Point(168, 102);
            this.KEY_COLON.Name = "KEY_COLON";
            this.KEY_COLON.Size = new System.Drawing.Size(43, 35);
            this.KEY_COLON.TabIndex = 21;
            this.KEY_COLON.Text = ":";
            this.KEY_COLON.UseVisualStyleBackColor = true;
            this.KEY_COLON.Click += new System.EventHandler(this.KEY_COLON_Click);
            // 
            // KEY_SQCLOSE
            // 
            this.KEY_SQCLOSE.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_SQCLOSE.Location = new System.Drawing.Point(126, 102);
            this.KEY_SQCLOSE.Name = "KEY_SQCLOSE";
            this.KEY_SQCLOSE.Size = new System.Drawing.Size(43, 35);
            this.KEY_SQCLOSE.TabIndex = 20;
            this.KEY_SQCLOSE.Text = "]";
            this.KEY_SQCLOSE.UseVisualStyleBackColor = true;
            this.KEY_SQCLOSE.Click += new System.EventHandler(this.KEY_SQCLOSE_Click);
            // 
            // KEY_SQOPEN
            // 
            this.KEY_SQOPEN.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_SQOPEN.Location = new System.Drawing.Point(84, 102);
            this.KEY_SQOPEN.Name = "KEY_SQOPEN";
            this.KEY_SQOPEN.Size = new System.Drawing.Size(43, 35);
            this.KEY_SQOPEN.TabIndex = 19;
            this.KEY_SQOPEN.Text = "[";
            this.KEY_SQOPEN.UseVisualStyleBackColor = true;
            this.KEY_SQOPEN.Click += new System.EventHandler(this.KEY_SQOPEN_Click);
            // 
            // KEY_PARCLOSE
            // 
            this.KEY_PARCLOSE.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_PARCLOSE.Location = new System.Drawing.Point(42, 102);
            this.KEY_PARCLOSE.Name = "KEY_PARCLOSE";
            this.KEY_PARCLOSE.Size = new System.Drawing.Size(43, 35);
            this.KEY_PARCLOSE.TabIndex = 18;
            this.KEY_PARCLOSE.Text = ")";
            this.KEY_PARCLOSE.UseVisualStyleBackColor = true;
            this.KEY_PARCLOSE.Click += new System.EventHandler(this.KEY_PARCLOSE_Click);
            // 
            // KEY_BACKTICK
            // 
            this.KEY_BACKTICK.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_BACKTICK.Location = new System.Drawing.Point(0, 136);
            this.KEY_BACKTICK.Name = "KEY_BACKTICK";
            this.KEY_BACKTICK.Size = new System.Drawing.Size(43, 35);
            this.KEY_BACKTICK.TabIndex = 35;
            this.KEY_BACKTICK.Text = "`";
            this.KEY_BACKTICK.UseVisualStyleBackColor = true;
            this.KEY_BACKTICK.Click += new System.EventHandler(this.KEY_BACKTICK_Click);
            // 
            // KEY_C20
            // 
            this.KEY_C20.Location = new System.Drawing.Point(336, 136);
            this.KEY_C20.Name = "KEY_C20";
            this.KEY_C20.Size = new System.Drawing.Size(63, 35);
            this.KEY_C20.TabIndex = 34;
            this.KEY_C20.Text = "c20";
            this.KEY_C20.UseVisualStyleBackColor = true;
            this.KEY_C20.Click += new System.EventHandler(this.KEY_C20_Click);
            // 
            // KEY_3
            // 
            this.KEY_3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_3.Location = new System.Drawing.Point(294, 136);
            this.KEY_3.Name = "KEY_3";
            this.KEY_3.Size = new System.Drawing.Size(43, 35);
            this.KEY_3.TabIndex = 33;
            this.KEY_3.Text = "3";
            this.KEY_3.UseVisualStyleBackColor = false;
            this.KEY_3.Click += new System.EventHandler(this.KEY_3_Click);
            // 
            // KEY_2
            // 
            this.KEY_2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_2.Location = new System.Drawing.Point(252, 136);
            this.KEY_2.Name = "KEY_2";
            this.KEY_2.Size = new System.Drawing.Size(43, 35);
            this.KEY_2.TabIndex = 32;
            this.KEY_2.Text = "2";
            this.KEY_2.UseVisualStyleBackColor = false;
            this.KEY_2.Click += new System.EventHandler(this.KEY_2_Click);
            // 
            // KEY_1
            // 
            this.KEY_1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_1.Location = new System.Drawing.Point(210, 136);
            this.KEY_1.Name = "KEY_1";
            this.KEY_1.Size = new System.Drawing.Size(43, 35);
            this.KEY_1.TabIndex = 31;
            this.KEY_1.Text = "1";
            this.KEY_1.UseVisualStyleBackColor = false;
            this.KEY_1.Click += new System.EventHandler(this.KEY_1_Click);
            // 
            // KEY_BACKSLASH
            // 
            this.KEY_BACKSLASH.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_BACKSLASH.Location = new System.Drawing.Point(168, 136);
            this.KEY_BACKSLASH.Name = "KEY_BACKSLASH";
            this.KEY_BACKSLASH.Size = new System.Drawing.Size(43, 35);
            this.KEY_BACKSLASH.TabIndex = 30;
            this.KEY_BACKSLASH.Text = "\\";
            this.KEY_BACKSLASH.UseVisualStyleBackColor = true;
            this.KEY_BACKSLASH.Click += new System.EventHandler(this.KEY_BACKSLASH_Click);
            // 
            // KEY_FORWARDSLASH
            // 
            this.KEY_FORWARDSLASH.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_FORWARDSLASH.Location = new System.Drawing.Point(126, 136);
            this.KEY_FORWARDSLASH.Name = "KEY_FORWARDSLASH";
            this.KEY_FORWARDSLASH.Size = new System.Drawing.Size(43, 35);
            this.KEY_FORWARDSLASH.TabIndex = 29;
            this.KEY_FORWARDSLASH.Text = "/";
            this.KEY_FORWARDSLASH.UseVisualStyleBackColor = true;
            this.KEY_FORWARDSLASH.Click += new System.EventHandler(this.KEY_FORWARDSLASH_Click);
            // 
            // KEY_APOSTROPHE
            // 
            this.KEY_APOSTROPHE.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_APOSTROPHE.Location = new System.Drawing.Point(84, 136);
            this.KEY_APOSTROPHE.Name = "KEY_APOSTROPHE";
            this.KEY_APOSTROPHE.Size = new System.Drawing.Size(43, 35);
            this.KEY_APOSTROPHE.TabIndex = 28;
            this.KEY_APOSTROPHE.Text = "\'";
            this.KEY_APOSTROPHE.UseVisualStyleBackColor = true;
            this.KEY_APOSTROPHE.Click += new System.EventHandler(this.KEY_APOSTROPHE_Click);
            // 
            // KEY_DOUBLEQUOTE
            // 
            this.KEY_DOUBLEQUOTE.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.KEY_DOUBLEQUOTE.Location = new System.Drawing.Point(42, 136);
            this.KEY_DOUBLEQUOTE.Name = "KEY_DOUBLEQUOTE";
            this.KEY_DOUBLEQUOTE.Size = new System.Drawing.Size(43, 35);
            this.KEY_DOUBLEQUOTE.TabIndex = 27;
            this.KEY_DOUBLEQUOTE.Text = "\"";
            this.KEY_DOUBLEQUOTE.UseVisualStyleBackColor = true;
            this.KEY_DOUBLEQUOTE.Click += new System.EventHandler(this.KEY_DOUBLEQUOTE_Click);
            // 
            // KEY_C6
            // 
            this.KEY_C6.Location = new System.Drawing.Point(0, 238);
            this.KEY_C6.Name = "KEY_C6";
            this.KEY_C6.Size = new System.Drawing.Size(43, 35);
            this.KEY_C6.TabIndex = 62;
            this.KEY_C6.Text = "c6";
            this.KEY_C6.UseVisualStyleBackColor = true;
            this.KEY_C6.Click += new System.EventHandler(this.KEY_C6_Click);
            // 
            // KEY_C23
            // 
            this.KEY_C23.Location = new System.Drawing.Point(336, 238);
            this.KEY_C23.Name = "KEY_C23";
            this.KEY_C23.Size = new System.Drawing.Size(63, 35);
            this.KEY_C23.TabIndex = 61;
            this.KEY_C23.Text = "c23";
            this.KEY_C23.UseVisualStyleBackColor = true;
            this.KEY_C23.Click += new System.EventHandler(this.KEY_C23_Click);
            // 
            // KEY_PGDN
            // 
            this.KEY_PGDN.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.KEY_PGDN.Location = new System.Drawing.Point(294, 238);
            this.KEY_PGDN.Name = "KEY_PGDN";
            this.KEY_PGDN.Size = new System.Drawing.Size(43, 35);
            this.KEY_PGDN.TabIndex = 60;
            this.KEY_PGDN.Text = "P-DN";
            this.KEY_PGDN.UseVisualStyleBackColor = false;
            this.KEY_PGDN.Click += new System.EventHandler(this.KEY_PGDN_Click);
            // 
            // KEY_0
            // 
            this.KEY_0.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_0.Location = new System.Drawing.Point(252, 238);
            this.KEY_0.Name = "KEY_0";
            this.KEY_0.Size = new System.Drawing.Size(43, 35);
            this.KEY_0.TabIndex = 59;
            this.KEY_0.Text = "0";
            this.KEY_0.UseVisualStyleBackColor = false;
            this.KEY_0.Click += new System.EventHandler(this.KEY_0_Click);
            // 
            // KEY_PGUP
            // 
            this.KEY_PGUP.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.KEY_PGUP.Location = new System.Drawing.Point(210, 238);
            this.KEY_PGUP.Name = "KEY_PGUP";
            this.KEY_PGUP.Size = new System.Drawing.Size(43, 35);
            this.KEY_PGUP.TabIndex = 58;
            this.KEY_PGUP.Text = "P-UP";
            this.KEY_PGUP.UseVisualStyleBackColor = false;
            this.KEY_PGUP.Click += new System.EventHandler(this.KEY_PGUP_Click);
            // 
            // KEY_C10
            // 
            this.KEY_C10.Location = new System.Drawing.Point(168, 238);
            this.KEY_C10.Name = "KEY_C10";
            this.KEY_C10.Size = new System.Drawing.Size(43, 35);
            this.KEY_C10.TabIndex = 57;
            this.KEY_C10.Text = "c10";
            this.KEY_C10.UseVisualStyleBackColor = true;
            this.KEY_C10.Click += new System.EventHandler(this.KEY_C10_Click);
            // 
            // KEY_C9
            // 
            this.KEY_C9.Location = new System.Drawing.Point(126, 238);
            this.KEY_C9.Name = "KEY_C9";
            this.KEY_C9.Size = new System.Drawing.Size(43, 35);
            this.KEY_C9.TabIndex = 56;
            this.KEY_C9.Text = "c9";
            this.KEY_C9.UseVisualStyleBackColor = true;
            this.KEY_C9.Click += new System.EventHandler(this.KEY_C9_Click);
            // 
            // KEY_C8
            // 
            this.KEY_C8.Location = new System.Drawing.Point(84, 238);
            this.KEY_C8.Name = "KEY_C8";
            this.KEY_C8.Size = new System.Drawing.Size(43, 35);
            this.KEY_C8.TabIndex = 55;
            this.KEY_C8.Text = "c8";
            this.KEY_C8.UseVisualStyleBackColor = true;
            this.KEY_C8.Click += new System.EventHandler(this.KEY_C8_Click);
            // 
            // KEY_C7
            // 
            this.KEY_C7.Location = new System.Drawing.Point(42, 238);
            this.KEY_C7.Name = "KEY_C7";
            this.KEY_C7.Size = new System.Drawing.Size(43, 35);
            this.KEY_C7.TabIndex = 54;
            this.KEY_C7.Text = "c7";
            this.KEY_C7.UseVisualStyleBackColor = true;
            this.KEY_C7.Click += new System.EventHandler(this.KEY_C7_Click);
            // 
            // KEY_C1
            // 
            this.KEY_C1.Location = new System.Drawing.Point(0, 204);
            this.KEY_C1.Name = "KEY_C1";
            this.KEY_C1.Size = new System.Drawing.Size(43, 35);
            this.KEY_C1.TabIndex = 53;
            this.KEY_C1.Text = "c1";
            this.KEY_C1.UseVisualStyleBackColor = true;
            this.KEY_C1.Click += new System.EventHandler(this.KEY_C1_Click);
            // 
            // KEY_C22
            // 
            this.KEY_C22.Location = new System.Drawing.Point(336, 204);
            this.KEY_C22.Name = "KEY_C22";
            this.KEY_C22.Size = new System.Drawing.Size(63, 35);
            this.KEY_C22.TabIndex = 52;
            this.KEY_C22.Text = "c22";
            this.KEY_C22.UseVisualStyleBackColor = true;
            this.KEY_C22.Click += new System.EventHandler(this.KEY_C22_Click);
            // 
            // KEY_9
            // 
            this.KEY_9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_9.Location = new System.Drawing.Point(294, 204);
            this.KEY_9.Name = "KEY_9";
            this.KEY_9.Size = new System.Drawing.Size(43, 35);
            this.KEY_9.TabIndex = 51;
            this.KEY_9.Text = "9";
            this.KEY_9.UseVisualStyleBackColor = false;
            this.KEY_9.Click += new System.EventHandler(this.KEY_9_Click);
            // 
            // KEY_8
            // 
            this.KEY_8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_8.Location = new System.Drawing.Point(252, 204);
            this.KEY_8.Name = "KEY_8";
            this.KEY_8.Size = new System.Drawing.Size(43, 35);
            this.KEY_8.TabIndex = 50;
            this.KEY_8.Text = "8";
            this.KEY_8.UseVisualStyleBackColor = false;
            this.KEY_8.Click += new System.EventHandler(this.KEY_8_Click);
            // 
            // KEY_7
            // 
            this.KEY_7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_7.Location = new System.Drawing.Point(210, 204);
            this.KEY_7.Name = "KEY_7";
            this.KEY_7.Size = new System.Drawing.Size(43, 35);
            this.KEY_7.TabIndex = 49;
            this.KEY_7.Text = "7";
            this.KEY_7.UseVisualStyleBackColor = false;
            this.KEY_7.Click += new System.EventHandler(this.KEY_7_Click);
            // 
            // KEY_C5
            // 
            this.KEY_C5.Location = new System.Drawing.Point(168, 204);
            this.KEY_C5.Name = "KEY_C5";
            this.KEY_C5.Size = new System.Drawing.Size(43, 35);
            this.KEY_C5.TabIndex = 48;
            this.KEY_C5.Text = "c5";
            this.KEY_C5.UseVisualStyleBackColor = true;
            this.KEY_C5.Click += new System.EventHandler(this.KEY_C5_Click);
            // 
            // KEY_C4
            // 
            this.KEY_C4.Location = new System.Drawing.Point(126, 204);
            this.KEY_C4.Name = "KEY_C4";
            this.KEY_C4.Size = new System.Drawing.Size(43, 35);
            this.KEY_C4.TabIndex = 47;
            this.KEY_C4.Text = "c4";
            this.KEY_C4.UseVisualStyleBackColor = true;
            this.KEY_C4.Click += new System.EventHandler(this.KEY_C4_Click);
            // 
            // KEY_C3
            // 
            this.KEY_C3.Location = new System.Drawing.Point(84, 204);
            this.KEY_C3.Name = "KEY_C3";
            this.KEY_C3.Size = new System.Drawing.Size(43, 35);
            this.KEY_C3.TabIndex = 46;
            this.KEY_C3.Text = "c3";
            this.KEY_C3.UseVisualStyleBackColor = true;
            this.KEY_C3.Click += new System.EventHandler(this.KEY_C3_Click);
            // 
            // KEY_C2
            // 
            this.KEY_C2.Location = new System.Drawing.Point(42, 204);
            this.KEY_C2.Name = "KEY_C2";
            this.KEY_C2.Size = new System.Drawing.Size(43, 35);
            this.KEY_C2.TabIndex = 45;
            this.KEY_C2.Text = "c2";
            this.KEY_C2.UseVisualStyleBackColor = true;
            this.KEY_C2.Click += new System.EventHandler(this.KEY_C2_Click);
            // 
            // KEY_PIPE
            // 
            this.KEY_PIPE.Location = new System.Drawing.Point(0, 170);
            this.KEY_PIPE.Name = "KEY_PIPE";
            this.KEY_PIPE.Size = new System.Drawing.Size(43, 35);
            this.KEY_PIPE.TabIndex = 44;
            this.KEY_PIPE.Text = "|";
            this.KEY_PIPE.UseVisualStyleBackColor = true;
            this.KEY_PIPE.Click += new System.EventHandler(this.KEY_PIPE_Click);
            // 
            // KEY_C21
            // 
            this.KEY_C21.Location = new System.Drawing.Point(336, 170);
            this.KEY_C21.Name = "KEY_C21";
            this.KEY_C21.Size = new System.Drawing.Size(63, 35);
            this.KEY_C21.TabIndex = 43;
            this.KEY_C21.Text = "c21";
            this.KEY_C21.UseVisualStyleBackColor = true;
            this.KEY_C21.Click += new System.EventHandler(this.KEY_C21_Click);
            // 
            // KEY_6
            // 
            this.KEY_6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_6.Location = new System.Drawing.Point(294, 170);
            this.KEY_6.Name = "KEY_6";
            this.KEY_6.Size = new System.Drawing.Size(43, 35);
            this.KEY_6.TabIndex = 42;
            this.KEY_6.Text = "6";
            this.KEY_6.UseVisualStyleBackColor = false;
            this.KEY_6.Click += new System.EventHandler(this.KEY_6_Click);
            // 
            // KEY_5
            // 
            this.KEY_5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_5.Location = new System.Drawing.Point(252, 170);
            this.KEY_5.Name = "KEY_5";
            this.KEY_5.Size = new System.Drawing.Size(43, 35);
            this.KEY_5.TabIndex = 41;
            this.KEY_5.Text = "5";
            this.KEY_5.UseVisualStyleBackColor = false;
            this.KEY_5.Click += new System.EventHandler(this.KEY_5_Click);
            // 
            // KEY_4
            // 
            this.KEY_4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_4.Location = new System.Drawing.Point(210, 170);
            this.KEY_4.Name = "KEY_4";
            this.KEY_4.Size = new System.Drawing.Size(43, 35);
            this.KEY_4.TabIndex = 40;
            this.KEY_4.Text = "4";
            this.KEY_4.UseVisualStyleBackColor = false;
            this.KEY_4.Click += new System.EventHandler(this.KEY_4_Click);
            // 
            // KEY_TWOPIPE
            // 
            this.KEY_TWOPIPE.Location = new System.Drawing.Point(168, 170);
            this.KEY_TWOPIPE.Name = "KEY_TWOPIPE";
            this.KEY_TWOPIPE.Size = new System.Drawing.Size(43, 35);
            this.KEY_TWOPIPE.TabIndex = 39;
            this.KEY_TWOPIPE.Text = "||";
            this.KEY_TWOPIPE.UseVisualStyleBackColor = true;
            this.KEY_TWOPIPE.Click += new System.EventHandler(this.KEY_TWOPIPE_Click);
            // 
            // KEY_NOTEQUALS
            // 
            this.KEY_NOTEQUALS.Location = new System.Drawing.Point(126, 170);
            this.KEY_NOTEQUALS.Name = "KEY_NOTEQUALS";
            this.KEY_NOTEQUALS.Size = new System.Drawing.Size(43, 35);
            this.KEY_NOTEQUALS.TabIndex = 38;
            this.KEY_NOTEQUALS.Text = "!=";
            this.KEY_NOTEQUALS.UseVisualStyleBackColor = true;
            this.KEY_NOTEQUALS.Click += new System.EventHandler(this.KEY_NOTEQUALS_Click);
            // 
            // KEY_TWOEQUALS
            // 
            this.KEY_TWOEQUALS.Location = new System.Drawing.Point(84, 170);
            this.KEY_TWOEQUALS.Name = "KEY_TWOEQUALS";
            this.KEY_TWOEQUALS.Size = new System.Drawing.Size(43, 35);
            this.KEY_TWOEQUALS.TabIndex = 37;
            this.KEY_TWOEQUALS.Text = "==";
            this.KEY_TWOEQUALS.UseVisualStyleBackColor = true;
            this.KEY_TWOEQUALS.Click += new System.EventHandler(this.KEY_TWOEQUALS_Click);
            // 
            // KEY_TWODOUBLEQUOTE
            // 
            this.KEY_TWODOUBLEQUOTE.Location = new System.Drawing.Point(42, 170);
            this.KEY_TWODOUBLEQUOTE.Name = "KEY_TWODOUBLEQUOTE";
            this.KEY_TWODOUBLEQUOTE.Size = new System.Drawing.Size(43, 35);
            this.KEY_TWODOUBLEQUOTE.TabIndex = 36;
            this.KEY_TWODOUBLEQUOTE.Text = "\"\"";
            this.KEY_TWODOUBLEQUOTE.UseVisualStyleBackColor = true;
            this.KEY_TWODOUBLEQUOTE.Click += new System.EventHandler(this.KEY_TWODOUBLEQUOTE_Click);
            // 
            // KEY_C16
            // 
            this.KEY_C16.Location = new System.Drawing.Point(0, 306);
            this.KEY_C16.Name = "KEY_C16";
            this.KEY_C16.Size = new System.Drawing.Size(43, 35);
            this.KEY_C16.TabIndex = 80;
            this.KEY_C16.Text = "c16";
            this.KEY_C16.UseVisualStyleBackColor = true;
            this.KEY_C16.Click += new System.EventHandler(this.KEY_C16_Click);
            // 
            // KEY_ENTER
            // 
            this.KEY_ENTER.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_ENTER.Location = new System.Drawing.Point(336, 306);
            this.KEY_ENTER.Name = "KEY_ENTER";
            this.KEY_ENTER.Size = new System.Drawing.Size(63, 35);
            this.KEY_ENTER.TabIndex = 79;
            this.KEY_ENTER.Text = "ENTER";
            this.KEY_ENTER.UseVisualStyleBackColor = false;
            this.KEY_ENTER.Click += new System.EventHandler(this.KEY_ENTER_Click);
            // 
            // KEY_RIGHT
            // 
            this.KEY_RIGHT.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.KEY_RIGHT.Location = new System.Drawing.Point(294, 306);
            this.KEY_RIGHT.Name = "KEY_RIGHT";
            this.KEY_RIGHT.Size = new System.Drawing.Size(43, 35);
            this.KEY_RIGHT.TabIndex = 78;
            this.KEY_RIGHT.Text = "RGT";
            this.KEY_RIGHT.UseVisualStyleBackColor = false;
            this.KEY_RIGHT.Click += new System.EventHandler(this.KEY_RIGHT_Click);
            // 
            // KEY_DOWN
            // 
            this.KEY_DOWN.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.KEY_DOWN.Location = new System.Drawing.Point(252, 306);
            this.KEY_DOWN.Name = "KEY_DOWN";
            this.KEY_DOWN.Size = new System.Drawing.Size(43, 35);
            this.KEY_DOWN.TabIndex = 77;
            this.KEY_DOWN.Text = "DN";
            this.KEY_DOWN.UseVisualStyleBackColor = false;
            this.KEY_DOWN.Click += new System.EventHandler(this.KEY_DOWN_Click);
            // 
            // KEY_LEFT
            // 
            this.KEY_LEFT.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.KEY_LEFT.Location = new System.Drawing.Point(210, 306);
            this.KEY_LEFT.Name = "KEY_LEFT";
            this.KEY_LEFT.Size = new System.Drawing.Size(43, 35);
            this.KEY_LEFT.TabIndex = 76;
            this.KEY_LEFT.Text = "LEFT";
            this.KEY_LEFT.UseVisualStyleBackColor = false;
            this.KEY_LEFT.Click += new System.EventHandler(this.KEY_LEFT_Click);
            // 
            // KEY_SPACE
            // 
            this.KEY_SPACE.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_SPACE.Location = new System.Drawing.Point(126, 306);
            this.KEY_SPACE.Name = "KEY_SPACE";
            this.KEY_SPACE.Size = new System.Drawing.Size(85, 35);
            this.KEY_SPACE.TabIndex = 74;
            this.KEY_SPACE.Text = "SPACE";
            this.KEY_SPACE.UseVisualStyleBackColor = false;
            this.KEY_SPACE.Click += new System.EventHandler(this.KEY_SPACE_Click);
            // 
            // KEY_C18
            // 
            this.KEY_C18.Location = new System.Drawing.Point(84, 306);
            this.KEY_C18.Name = "KEY_C18";
            this.KEY_C18.Size = new System.Drawing.Size(43, 35);
            this.KEY_C18.TabIndex = 73;
            this.KEY_C18.Text = "c18";
            this.KEY_C18.UseVisualStyleBackColor = true;
            this.KEY_C18.Click += new System.EventHandler(this.KEY_C18_Click);
            // 
            // KEY_C17
            // 
            this.KEY_C17.Location = new System.Drawing.Point(42, 306);
            this.KEY_C17.Name = "KEY_C17";
            this.KEY_C17.Size = new System.Drawing.Size(43, 35);
            this.KEY_C17.TabIndex = 72;
            this.KEY_C17.Text = "c17";
            this.KEY_C17.UseVisualStyleBackColor = true;
            this.KEY_C17.Click += new System.EventHandler(this.KEY_C17_Click);
            // 
            // KEY_C11
            // 
            this.KEY_C11.Location = new System.Drawing.Point(0, 272);
            this.KEY_C11.Name = "KEY_C11";
            this.KEY_C11.Size = new System.Drawing.Size(43, 35);
            this.KEY_C11.TabIndex = 71;
            this.KEY_C11.Text = "c11";
            this.KEY_C11.UseVisualStyleBackColor = true;
            this.KEY_C11.Click += new System.EventHandler(this.KEY_C11_Click);
            // 
            // KEY_C24
            // 
            this.KEY_C24.Location = new System.Drawing.Point(336, 272);
            this.KEY_C24.Name = "KEY_C24";
            this.KEY_C24.Size = new System.Drawing.Size(63, 35);
            this.KEY_C24.TabIndex = 70;
            this.KEY_C24.Text = "c24";
            this.KEY_C24.UseVisualStyleBackColor = true;
            this.KEY_C24.Click += new System.EventHandler(this.KEY_C24_Click);
            // 
            // KEY_END
            // 
            this.KEY_END.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.KEY_END.Location = new System.Drawing.Point(294, 272);
            this.KEY_END.Name = "KEY_END";
            this.KEY_END.Size = new System.Drawing.Size(43, 35);
            this.KEY_END.TabIndex = 69;
            this.KEY_END.Text = "END";
            this.KEY_END.UseVisualStyleBackColor = false;
            this.KEY_END.Click += new System.EventHandler(this.KEY_END_Click);
            // 
            // KEY_UP
            // 
            this.KEY_UP.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.KEY_UP.Location = new System.Drawing.Point(252, 272);
            this.KEY_UP.Name = "KEY_UP";
            this.KEY_UP.Size = new System.Drawing.Size(43, 35);
            this.KEY_UP.TabIndex = 68;
            this.KEY_UP.Text = "UP";
            this.KEY_UP.UseVisualStyleBackColor = false;
            this.KEY_UP.Click += new System.EventHandler(this.KEY_UP_Click);
            // 
            // KEY_HOME
            // 
            this.KEY_HOME.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.KEY_HOME.Location = new System.Drawing.Point(210, 272);
            this.KEY_HOME.Name = "KEY_HOME";
            this.KEY_HOME.Size = new System.Drawing.Size(43, 35);
            this.KEY_HOME.TabIndex = 67;
            this.KEY_HOME.Text = "HME";
            this.KEY_HOME.UseVisualStyleBackColor = false;
            this.KEY_HOME.Click += new System.EventHandler(this.KEY_HOME_Click);
            // 
            // KEY_C15
            // 
            this.KEY_C15.Location = new System.Drawing.Point(168, 272);
            this.KEY_C15.Name = "KEY_C15";
            this.KEY_C15.Size = new System.Drawing.Size(43, 35);
            this.KEY_C15.TabIndex = 66;
            this.KEY_C15.Text = "c15";
            this.KEY_C15.UseVisualStyleBackColor = true;
            this.KEY_C15.Click += new System.EventHandler(this.KEY_C15_Click);
            // 
            // KEY_C14
            // 
            this.KEY_C14.Location = new System.Drawing.Point(126, 272);
            this.KEY_C14.Name = "KEY_C14";
            this.KEY_C14.Size = new System.Drawing.Size(43, 35);
            this.KEY_C14.TabIndex = 65;
            this.KEY_C14.Text = "c14";
            this.KEY_C14.UseVisualStyleBackColor = true;
            this.KEY_C14.Click += new System.EventHandler(this.KEY_C14_Click);
            // 
            // KEY_C13
            // 
            this.KEY_C13.Location = new System.Drawing.Point(84, 272);
            this.KEY_C13.Name = "KEY_C13";
            this.KEY_C13.Size = new System.Drawing.Size(43, 35);
            this.KEY_C13.TabIndex = 64;
            this.KEY_C13.Text = "c13";
            this.KEY_C13.UseVisualStyleBackColor = true;
            this.KEY_C13.Click += new System.EventHandler(this.KEY_C13_Click);
            // 
            // KEY_C12
            // 
            this.KEY_C12.Location = new System.Drawing.Point(42, 272);
            this.KEY_C12.Name = "KEY_C12";
            this.KEY_C12.Size = new System.Drawing.Size(43, 35);
            this.KEY_C12.TabIndex = 63;
            this.KEY_C12.Text = "c12";
            this.KEY_C12.UseVisualStyleBackColor = true;
            this.KEY_C12.Click += new System.EventHandler(this.KEY_C12_Click);
            // 
            // KEY_CTRLC
            // 
            this.KEY_CTRLC.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_CTRLC.Location = new System.Drawing.Point(105, 0);
            this.KEY_CTRLC.Name = "KEY_CTRLC";
            this.KEY_CTRLC.Size = new System.Drawing.Size(64, 35);
            this.KEY_CTRLC.TabIndex = 82;
            this.KEY_CTRLC.Text = "CTRL+C";
            this.KEY_CTRLC.UseVisualStyleBackColor = false;
            this.KEY_CTRLC.Click += new System.EventHandler(this.KEY_CTRLC_Click);
            // 
            // KEY_CTRLS
            // 
            this.KEY_CTRLS.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_CTRLS.Location = new System.Drawing.Point(42, 0);
            this.KEY_CTRLS.Name = "KEY_CTRLS";
            this.KEY_CTRLS.Size = new System.Drawing.Size(64, 35);
            this.KEY_CTRLS.TabIndex = 83;
            this.KEY_CTRLS.Text = "CTRL+S";
            this.KEY_CTRLS.UseVisualStyleBackColor = false;
            this.KEY_CTRLS.Click += new System.EventHandler(this.KEY_CTRLS_Click);
            // 
            // KEY_CTRLX
            // 
            this.KEY_CTRLX.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_CTRLX.Location = new System.Drawing.Point(168, 0);
            this.KEY_CTRLX.Name = "KEY_CTRLX";
            this.KEY_CTRLX.Size = new System.Drawing.Size(64, 35);
            this.KEY_CTRLX.TabIndex = 85;
            this.KEY_CTRLX.Text = "CTRL+X";
            this.KEY_CTRLX.UseVisualStyleBackColor = false;
            this.KEY_CTRLX.Click += new System.EventHandler(this.KEY_CTRLX_Click);
            // 
            // KEY_CTRLV
            // 
            this.KEY_CTRLV.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_CTRLV.Location = new System.Drawing.Point(231, 0);
            this.KEY_CTRLV.Name = "KEY_CTRLV";
            this.KEY_CTRLV.Size = new System.Drawing.Size(64, 35);
            this.KEY_CTRLV.TabIndex = 84;
            this.KEY_CTRLV.Text = "CTRL+V";
            this.KEY_CTRLV.UseVisualStyleBackColor = false;
            this.KEY_CTRLV.Click += new System.EventHandler(this.KEY_CTRLV_Click);
            // 
            // KEY_CTRLA
            // 
            this.KEY_CTRLA.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_CTRLA.Location = new System.Drawing.Point(294, 0);
            this.KEY_CTRLA.Name = "KEY_CTRLA";
            this.KEY_CTRLA.Size = new System.Drawing.Size(64, 35);
            this.KEY_CTRLA.TabIndex = 87;
            this.KEY_CTRLA.Text = "CTRL+A";
            this.KEY_CTRLA.UseVisualStyleBackColor = false;
            this.KEY_CTRLA.Click += new System.EventHandler(this.KEY_CTRLA_Click);
            // 
            // KEY_OPT
            // 
            this.KEY_OPT.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.KEY_OPT.Location = new System.Drawing.Point(357, 0);
            this.KEY_OPT.Name = "KEY_OPT";
            this.KEY_OPT.Size = new System.Drawing.Size(42, 35);
            this.KEY_OPT.TabIndex = 88;
            this.KEY_OPT.Text = "OPT";
            this.KEY_OPT.UseVisualStyleBackColor = false;
            this.KEY_OPT.Click += new System.EventHandler(this.button87_Click);
            // 
            // KEY_ESC
            // 
            this.KEY_ESC.BackColor = System.Drawing.SystemColors.ControlLight;
            this.KEY_ESC.Location = new System.Drawing.Point(0, 0);
            this.KEY_ESC.Name = "KEY_ESC";
            this.KEY_ESC.Size = new System.Drawing.Size(43, 35);
            this.KEY_ESC.TabIndex = 89;
            this.KEY_ESC.Text = "ESC";
            this.KEY_ESC.UseVisualStyleBackColor = false;
            this.KEY_ESC.Click += new System.EventHandler(this.KEY_ESC_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 343);
            this.Controls.Add(this.KEY_ESC);
            this.Controls.Add(this.KEY_OPT);
            this.Controls.Add(this.KEY_CTRLA);
            this.Controls.Add(this.KEY_CTRLX);
            this.Controls.Add(this.KEY_CTRLV);
            this.Controls.Add(this.KEY_CTRLS);
            this.Controls.Add(this.KEY_CTRLC);
            this.Controls.Add(this.KEY_C16);
            this.Controls.Add(this.KEY_ENTER);
            this.Controls.Add(this.KEY_RIGHT);
            this.Controls.Add(this.KEY_DOWN);
            this.Controls.Add(this.KEY_LEFT);
            this.Controls.Add(this.KEY_SPACE);
            this.Controls.Add(this.KEY_C18);
            this.Controls.Add(this.KEY_C17);
            this.Controls.Add(this.KEY_C11);
            this.Controls.Add(this.KEY_C24);
            this.Controls.Add(this.KEY_END);
            this.Controls.Add(this.KEY_UP);
            this.Controls.Add(this.KEY_HOME);
            this.Controls.Add(this.KEY_C15);
            this.Controls.Add(this.KEY_C14);
            this.Controls.Add(this.KEY_C13);
            this.Controls.Add(this.KEY_C12);
            this.Controls.Add(this.KEY_C6);
            this.Controls.Add(this.KEY_C23);
            this.Controls.Add(this.KEY_PGDN);
            this.Controls.Add(this.KEY_0);
            this.Controls.Add(this.KEY_PGUP);
            this.Controls.Add(this.KEY_C10);
            this.Controls.Add(this.KEY_C9);
            this.Controls.Add(this.KEY_C8);
            this.Controls.Add(this.KEY_C7);
            this.Controls.Add(this.KEY_C1);
            this.Controls.Add(this.KEY_C22);
            this.Controls.Add(this.KEY_9);
            this.Controls.Add(this.KEY_8);
            this.Controls.Add(this.KEY_7);
            this.Controls.Add(this.KEY_C5);
            this.Controls.Add(this.KEY_C4);
            this.Controls.Add(this.KEY_C3);
            this.Controls.Add(this.KEY_C2);
            this.Controls.Add(this.KEY_PIPE);
            this.Controls.Add(this.KEY_C21);
            this.Controls.Add(this.KEY_6);
            this.Controls.Add(this.KEY_5);
            this.Controls.Add(this.KEY_4);
            this.Controls.Add(this.KEY_TWOPIPE);
            this.Controls.Add(this.KEY_NOTEQUALS);
            this.Controls.Add(this.KEY_TWOEQUALS);
            this.Controls.Add(this.KEY_TWODOUBLEQUOTE);
            this.Controls.Add(this.KEY_BACKTICK);
            this.Controls.Add(this.KEY_C20);
            this.Controls.Add(this.KEY_3);
            this.Controls.Add(this.KEY_2);
            this.Controls.Add(this.KEY_1);
            this.Controls.Add(this.KEY_BACKSLASH);
            this.Controls.Add(this.KEY_FORWARDSLASH);
            this.Controls.Add(this.KEY_APOSTROPHE);
            this.Controls.Add(this.KEY_DOUBLEQUOTE);
            this.Controls.Add(this.KEY_PAROPEN);
            this.Controls.Add(this.KEY_C19);
            this.Controls.Add(this.KEY_COMMA);
            this.Controls.Add(this.KEY_PERIOD);
            this.Controls.Add(this.KEY_SEMICOLON);
            this.Controls.Add(this.KEY_COLON);
            this.Controls.Add(this.KEY_SQCLOSE);
            this.Controls.Add(this.KEY_SQOPEN);
            this.Controls.Add(this.KEY_PARCLOSE);
            this.Controls.Add(this.KEY_LESSTHAN);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.KEY_EQUALS);
            this.Controls.Add(this.KEY_PLUS);
            this.Controls.Add(this.KEY_UNDERSCORE);
            this.Controls.Add(this.KEY_HYPHEN);
            this.Controls.Add(this.KEY_CURLYCLOSE);
            this.Controls.Add(this.KEY_CURLYOPEN);
            this.Controls.Add(this.KEY_GREATERTHAN);
            this.Controls.Add(this.KEY_BACKSPACE);
            this.Controls.Add(this.KEY_ASTERISK);
            this.Controls.Add(this.KEY_AMP);
            this.Controls.Add(this.KEY_PERCENT);
            this.Controls.Add(this.KEY_DOLLAR);
            this.Controls.Add(this.KEY_HASH);
            this.Controls.Add(this.KEY_AT);
            this.Controls.Add(this.KEY_EXCLAMATION);
            this.Controls.Add(this.KEY_TAB);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PenCoding Keyboard";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Button KEY_TAB;
        private Button KEY_EXCLAMATION;
        private Button KEY_AT;
        private Button KEY_DOLLAR;
        private Button KEY_HASH;
        private Button KEY_BACKSPACE;
        private Button KEY_ASTERISK;
        private Button KEY_AMP;
        private Button KEY_PERCENT;
        private Button button10;
        private Button KEY_EQUALS;
        private Button KEY_PLUS;
        private Button KEY_UNDERSCORE;
        private Button KEY_HYPHEN;
        private Button KEY_CURLYCLOSE;
        private Button KEY_CURLYOPEN;
        private Button KEY_GREATERTHAN;
        private Button KEY_LESSTHAN;
        private Button KEY_PAROPEN;
        private Button KEY_C19;
        private Button KEY_COMMA;
        private Button KEY_PERIOD;
        private Button KEY_SEMICOLON;
        private Button KEY_COLON;
        private Button KEY_SQCLOSE;
        private Button KEY_SQOPEN;
        private Button KEY_PARCLOSE;
        private Button KEY_BACKTICK;
        private Button KEY_C20;
        private Button KEY_3;
        private Button KEY_2;
        private Button KEY_1;
        private Button KEY_BACKSLASH;
        private Button KEY_FORWARDSLASH;
        private Button KEY_APOSTROPHE;
        private Button KEY_DOUBLEQUOTE;
        private Button KEY_C6;
        private Button KEY_C23;
        private Button KEY_PGDN;
        private Button KEY_0;
        private Button KEY_PGUP;
        private Button KEY_C10;
        private Button KEY_C9;
        private Button KEY_C8;
        private Button KEY_C7;
        private Button KEY_C1;
        private Button KEY_C22;
        private Button KEY_9;
        private Button KEY_8;
        private Button KEY_7;
        private Button KEY_C5;
        private Button KEY_C4;
        private Button KEY_C3;
        private Button KEY_C2;
        private Button KEY_PIPE;
        private Button KEY_C21;
        private Button KEY_6;
        private Button KEY_5;
        private Button KEY_4;
        private Button KEY_TWOPIPE;
        private Button KEY_NOTEQUALS;
        private Button KEY_TWOEQUALS;
        private Button KEY_TWODOUBLEQUOTE;
        private Button KEY_C16;
        private Button KEY_ENTER;
        private Button KEY_RIGHT;
        private Button KEY_DOWN;
        private Button KEY_LEFT;
        private Button KEY_SPACE;
        private Button KEY_C18;
        private Button KEY_C17;
        private Button KEY_C11;
        private Button KEY_C24;
        private Button KEY_END;
        private Button KEY_UP;
        private Button KEY_HOME;
        private Button KEY_C15;
        private Button KEY_C14;
        private Button KEY_C13;
        private Button KEY_C12;
        private Button KEY_CTRLC;
        private Button KEY_CTRLS;
        private Button KEY_CTRLX;
        private Button KEY_CTRLV;
        private Button KEY_CTRLA;
        private Button KEY_OPT;
        private Button KEY_ESC;
    }
}