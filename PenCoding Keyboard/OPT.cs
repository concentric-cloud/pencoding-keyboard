﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PenCoding_Keyboard
{
    public partial class OPT : Form
    {
        public OPT()
        {
            InitializeComponent();
        }

        string execPath = AppDomain.CurrentDomain.BaseDirectory;

        private void button1_Click(object sender, EventArgs e)
        {

            ///////////////////////////////////// SAVING SETTINGS

            /////// Startup Keymap

            if(File.Exists(execPath + "\\" + textBox1.Text + ".key"))
            {
                File.WriteAllText(execPath + "\\Options.conf", "StartupKeymap:" + textBox1.Text);
            }
            else
            {
                MessageBox.Show("The Startup Keymap you specified was not found, please check the spelling and ensure the file name is specified without the .key");
                return;
            }


            //// FINISHED
            this.Close();
        }

        private void OPT_Load(object sender, EventArgs e)
        {
            var settingsFile = File.ReadAllLines(execPath + "\\Options.conf");
            foreach(string setting in settingsFile)
            {
                var settingSplit = setting.Split(':');
                switch(settingSplit[0])
                {
                    case "StartupKeymap":
                        textBox1.Text = settingSplit[1];
                        break;
                }
            }
        }
    }
}
