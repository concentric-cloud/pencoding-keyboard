﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PenCoding_Keyboard
{
    public partial class SWITCH : Form
    {
        public SWITCH()
        {
            InitializeComponent();
        }

        public Form1 MyParent { get; set; }


        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void SWITCH_Load(object sender, EventArgs e)
        {
            string execPath = AppDomain.CurrentDomain.BaseDirectory;
            string[] fileEntries = Directory.GetFiles(execPath);
            foreach(string fileEntry in fileEntries)
            {
                var splitPath = fileEntry.Split("\\");
                var fileName = splitPath[splitPath.Length - 1];

                var splitFilename = fileName.Split(".");
                var extn = splitFilename[splitFilename.Length - 1];  

                if(extn=="key")
                {
                    listBox1.Items.Add(splitFilename[0]);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string filename = listBox1.Text;
            this.Close();
            this.MyParent.switchFile(filename);
        }
    }
}
