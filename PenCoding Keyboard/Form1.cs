using System.Runtime.InteropServices;

namespace PenCoding_Keyboard
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams param = base.CreateParams;
                param.ExStyle |= 0x08000000;
                return param;
            }
        }

        public Dictionary<string,Button> keyDictionary = new Dictionary<string,Button>();
        public Dictionary<string,string> keyActions = new Dictionary<string,string>();

        private void loadKey(string keyName, string keyLabel, string sendKey)
        {
            try
            {
                var keyObj = keyDictionary[keyName];
                keyObj.Text = keyLabel;

                if (keyActions.ContainsKey(keyName))
                {
                    keyActions.Remove(keyName);
                }

                keyActions.Add(keyName, sendKey);

            }
            catch
            {
                MessageBox.Show("The keyname '" + keyName + "' is not a mappable key");
            }

        }

        private void pressKey(Object sender)
        {
            var button = (Button)sender;
            var controlName = button.Name;
            var sendKey = keyActions[controlName];
            SendKeys.Send(sendKey);
        }

        public void switchFile(string filePath)
        {
            keyActions.Clear();
            processFile(filePath);
        }

        string execPath = AppDomain.CurrentDomain.BaseDirectory;

        public void processFile(string file)
        {
            var keyfile = File.ReadAllLines(execPath + "\\" + file+".key");

            foreach (string keyline in keyfile)
            {
                var splitCommas = keyline.Split(",");

                string keyName = splitCommas[0];
                var keyLabel = splitCommas[1];
                
                if(keyLabel=="{COMMA}")
                {
                    keyLabel = ",";
                }

                var keyAction = splitCommas[2];

                if(keyAction=="{COMMA}")
                {
                    keyAction = ",";
                }

                loadKey(keyName, keyLabel, keyAction);
            }
        }

        private void loadButtonObjects()
        {
            
            keyDictionary.Add("KEY_ESC", KEY_ESC);
            keyDictionary.Add("KEY_CTRLC", KEY_CTRLC);
            keyDictionary.Add("KEY_CTRLS", KEY_CTRLS);
            keyDictionary.Add("KEY_CTRLX", KEY_CTRLX);
            keyDictionary.Add("KEY_CTRLV", KEY_CTRLV);
            keyDictionary.Add("KEY_CTRLA", KEY_CTRLA);
            keyDictionary.Add("KEY_TAB", KEY_TAB);
            keyDictionary.Add("KEY_EXCLAMATION", KEY_EXCLAMATION);
            keyDictionary.Add("KEY_AT", KEY_AT);
            keyDictionary.Add("KEY_HASH", KEY_HASH);
            keyDictionary.Add("KEY_DOLLAR", KEY_DOLLAR);
            keyDictionary.Add("KEY_PERCENT", KEY_PERCENT);
            keyDictionary.Add("KEY_AMP", KEY_AMP);
            keyDictionary.Add("KEY_ASTERISK", KEY_ASTERISK);
            keyDictionary.Add("KEY_BACKSPACE", KEY_BACKSPACE);
            keyDictionary.Add("KEY_LESSTHAN", KEY_LESSTHAN);
            keyDictionary.Add("KEY_GREATERTHAN", KEY_GREATERTHAN);
            keyDictionary.Add("KEY_CURLYOPEN", KEY_CURLYOPEN);
            keyDictionary.Add("KEY_CURLYCLOSE", KEY_CURLYCLOSE);
            keyDictionary.Add("KEY_HYPHEN", KEY_HYPHEN);
            keyDictionary.Add("KEY_UNDERSCORE", KEY_UNDERSCORE);
            keyDictionary.Add("KEY_PLUS", KEY_PLUS);
            keyDictionary.Add("KEY_EQUALS", KEY_EQUALS);
            keyDictionary.Add("KEY_PAROPEN", KEY_PAROPEN);
            keyDictionary.Add("KEY_PARCLOSE", KEY_PARCLOSE);
            keyDictionary.Add("KEY_SQOPEN", KEY_SQOPEN);
            keyDictionary.Add("KEY_SQCLOSE", KEY_SQCLOSE);
            keyDictionary.Add("KEY_COLON", KEY_COLON);
            keyDictionary.Add("KEY_SEMICOLON", KEY_SEMICOLON);
            keyDictionary.Add("KEY_PERIOD", KEY_PERIOD);
            keyDictionary.Add("KEY_COMMA", KEY_COMMA);
            keyDictionary.Add("KEY_C19", KEY_C19);
            keyDictionary.Add("KEY_BACKTICK", KEY_BACKTICK);
            keyDictionary.Add("KEY_DOUBLEQUOTE", KEY_DOUBLEQUOTE);
            keyDictionary.Add("KEY_APOSTROPHE", KEY_APOSTROPHE);
            keyDictionary.Add("KEY_FORWARDSLASH", KEY_FORWARDSLASH);
            keyDictionary.Add("KEY_BACKSLASH", KEY_BACKSLASH);
            keyDictionary.Add("KEY_1", KEY_1);
            keyDictionary.Add("KEY_2", KEY_2);
            keyDictionary.Add("KEY_3", KEY_3);
            keyDictionary.Add("KEY_C20", KEY_C20);
            keyDictionary.Add("KEY_PIPE", KEY_PIPE);
            keyDictionary.Add("KEY_TWODOUBLEQUOTE", KEY_TWODOUBLEQUOTE);
            keyDictionary.Add("KEY_TWOEQUALS", KEY_TWOEQUALS);
            keyDictionary.Add("KEY_NOTEQUALS", KEY_NOTEQUALS);
            keyDictionary.Add("KEY_TWOPIPE", KEY_TWOPIPE);
            keyDictionary.Add("KEY_4", KEY_4);
            keyDictionary.Add("KEY_5", KEY_5);
            keyDictionary.Add("KEY_6", KEY_6);
            keyDictionary.Add("KEY_C21", KEY_C21);
            keyDictionary.Add("KEY_C1", KEY_C1);
            keyDictionary.Add("KEY_C2", KEY_C2);
            keyDictionary.Add("KEY_C3", KEY_C3);
            keyDictionary.Add("KEY_C4", KEY_C4);
            keyDictionary.Add("KEY_C5", KEY_C5);
            keyDictionary.Add("KEY_7", KEY_7);
            keyDictionary.Add("KEY_8", KEY_8);
            keyDictionary.Add("KEY_9", KEY_9);
            keyDictionary.Add("KEY_C22", KEY_C22);
            keyDictionary.Add("KEY_C6", KEY_C6);
            keyDictionary.Add("KEY_C7", KEY_C7);
            keyDictionary.Add("KEY_C8", KEY_C8);
            keyDictionary.Add("KEY_C9", KEY_C9);
            keyDictionary.Add("KEY_C10", KEY_C10);
            keyDictionary.Add("KEY_PGUP", KEY_PGUP);
            keyDictionary.Add("KEY_0", KEY_0);
            keyDictionary.Add("KEY_PGDN", KEY_PGDN);
            keyDictionary.Add("KEY_C23", KEY_C23);
            keyDictionary.Add("KEY_C11", KEY_C11);
            keyDictionary.Add("KEY_C12", KEY_C12);
            keyDictionary.Add("KEY_C13", KEY_C13);
            keyDictionary.Add("KEY_C14", KEY_C14);
            keyDictionary.Add("KEY_C15", KEY_C15);
            keyDictionary.Add("KEY_HOME", KEY_HOME);
            keyDictionary.Add("KEY_UP", KEY_UP);
            keyDictionary.Add("KEY_END", KEY_END);
            keyDictionary.Add("KEY_C24", KEY_C24);
            keyDictionary.Add("KEY_C16", KEY_C16);
            keyDictionary.Add("KEY_C17", KEY_C17);
            keyDictionary.Add("KEY_C18", KEY_C18);
            keyDictionary.Add("KEY_SPACE", KEY_SPACE);
            keyDictionary.Add("KEY_LEFT", KEY_LEFT);
            keyDictionary.Add("KEY_DOWN", KEY_DOWN);
            keyDictionary.Add("KEY_RIGHT", KEY_RIGHT);
            keyDictionary.Add("KEY_ENTER", KEY_ENTER);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - Size.Width,
                                      workingArea.Bottom - Size.Height);

            loadButtonObjects();

            var settingsFile = File.ReadAllLines(execPath + "\\Options.conf");
            foreach (string setting in settingsFile)
            {
                var settingSplit = setting.Split(':');
                switch (settingSplit[0])
                {
                    case "StartupKeymap":
                        processFile(settingSplit[1]);
                        break;
                }
            }

            


        }

        

        private void button87_Click(object sender, EventArgs e)
        {
            var OPT = new OPT();
            OPT.Show();

        }

        private void KEY_ESC_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_1_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_CTRLS_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_CTRLC_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_CTRLX_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_CTRLV_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_CTRLA_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_TAB_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_EXCLAMATION_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_AT_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_HASH_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_DOLLAR_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_PERCENT_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_AMP_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_ASTERISK_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_BACKSPACE_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_LESSTHAN_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_GREATERTHAN_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_CURLYOPEN_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_CURLYCLOSE_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_HYPHEN_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_UNDERSCORE_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_PLUS_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_EQUALS_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_PAROPEN_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_PARCLOSE_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_SQOPEN_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_SQCLOSE_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_COLON_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_SEMICOLON_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_PERIOD_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_COMMA_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C19_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_BACKTICK_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_DOUBLEQUOTE_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_APOSTROPHE_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_FORWARDSLASH_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_BACKSLASH_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_2_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_3_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C20_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_PIPE_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_TWODOUBLEQUOTE_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_TWOEQUALS_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_NOTEQUALS_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_TWOPIPE_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_4_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_5_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_6_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C21_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C1_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C2_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C3_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C4_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C5_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_7_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_8_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_9_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C22_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C6_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C7_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C8_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C9_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C10_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_PGUP_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_0_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_PGDN_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C23_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C11_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C12_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C13_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C14_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C15_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_HOME_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_UP_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_END_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C24_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C16_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C17_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_C18_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_SPACE_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_LEFT_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_DOWN_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_RIGHT_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void KEY_ENTER_Click(object sender, EventArgs e)
        {
            pressKey(sender);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            var SWITCH = new SWITCH();
            SWITCH.MyParent = this;
            SWITCH.Show();
        }
    }
}